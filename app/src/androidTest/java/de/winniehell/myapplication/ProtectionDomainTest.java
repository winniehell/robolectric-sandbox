package de.winniehell.myapplication;

import android.app.Application;
import android.test.ApplicationTestCase;

import org.junit.Assert;
import org.junit.Test;

public class ProtectionDomainTest extends ApplicationTestCase<Application> {
    public ProtectionDomainTest() {
        super(Application.class);
    }

    @Test
    public void testGetProtectionDomain() {
        Assert.assertNull(MainActivity.class.getProtectionDomain());
    }
}