package de.winniehell.myapplication;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class)
public class ProtectionDomainTest {

    @Test
    public void testGetProtectionDomain() {
        Assert.assertNull(MainActivity.class.getProtectionDomain());
    }
}